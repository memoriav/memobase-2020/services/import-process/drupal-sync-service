/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.InstitutionArbitrator.completeInstitution
import ch.memobase.InstitutionArbitrator.dataPath
import ch.memobase.InstitutionArbitrator.minimalInstitution
import ch.memobase.RecordSetArbitrator.completeRecordSet
import ch.memobase.RecordSetArbitrator.minimalRecordSet
import ch.memobase.rdf.NS
import io.kotest.core.spec.style.FunSpec
import io.kotest.property.forAll
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import java.io.FileOutputStream

class PropertyTests : FunSpec({
    test("test institution transformation minimal shape") {
        val directory = "minimal"
        val settings = Util.loadSettings("basicTestConfiguration.yml")
        val rdfTransformer = RdfTransformer(settings.appSettings)
        minimalInstitution.forAll {
            val output = rdfTransformer.createInstitution(it)
            val model = output.second
            val validator = RdfValidation("$dataPath/institution/$directory/shape.ttl")
            val validation = validator.validate(model)
            model.setNsPrefixes(NS.prefixMapping)
            RDFDataMgr.write(
                FileOutputStream("$dataPath/institution/$directory/output.ttl"),
                model,
                Lang.TTL
            )
            validation.first
        }
    }

    test("test institution transformation complete shape") {
        val directory = "complete"
        val settings = Util.loadSettings("basicTestConfiguration.yml")
        val rdfTransformer = RdfTransformer(settings.appSettings)
        completeInstitution.forAll {
            val output = rdfTransformer.createInstitution(it)
            val model = output.second
            val validator = RdfValidation("$dataPath/institution/$directory/shape.ttl")
            val validation = validator.validate(model)
            model.setNsPrefixes(NS.prefixMapping)
            RDFDataMgr.write(
                FileOutputStream("$dataPath/institution/$directory/output.ttl"),
                model,
                Lang.TTL
            )
            FileOutputStream("$dataPath/institution/$directory/result.ttl").bufferedWriter()
                .use { bufferedWriter -> bufferedWriter.write(validation.second) }
            validation.first
        }
    }

    test("test record set transformation minimal shape") {
        val directory = "minimal"
        val settings = Util.loadSettings("basicTestConfiguration.yml")
        val rdfTransformer = RdfTransformer(settings.appSettings)
        minimalRecordSet.forAll {
            val output = rdfTransformer.createRecordSet(it)
            val model = output.second
            val validator = RdfValidation("$dataPath/recordSets/$directory/shape.ttl")
            val validation = validator.validate(model)
            model.setNsPrefixes(NS.prefixMapping)
            RDFDataMgr.write(
                FileOutputStream("$dataPath/recordSets/$directory/output.ttl"),
                model,
                Lang.TTL
            )
            FileOutputStream("$dataPath/recordSets/$directory/result.ttl").bufferedWriter()
                .use { bufferedWriter -> bufferedWriter.write(validation.second) }
            validation.first
        }
    }


    xtest("test record set transformation complete shape") {
        val directory = "complete"
        val settings = Util.loadSettings("basicTestConfiguration.yml")
        val rdfTransformer = RdfTransformer(settings.appSettings)
        completeRecordSet.forAll {
            val output = rdfTransformer.createRecordSet(it)
            val model = output.second
            val validator = RdfValidation("$dataPath/recordSets/$directory/shape.ttl")
            val validation = validator.validate(model)
            model.setNsPrefixes(NS.prefixMapping)
            RDFDataMgr.write(
                FileOutputStream("$dataPath/recordSets/$directory/output.ttl"),
                model,
                Lang.TTL
            )
            FileOutputStream("$dataPath/recordSets/$directory/result.ttl").bufferedWriter()
                .use { bufferedWriter -> bufferedWriter.write(validation.second) }
            validation.first
        }
    }
})