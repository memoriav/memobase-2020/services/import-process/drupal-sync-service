/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.model.*
import io.kotest.property.Arb
import io.kotest.property.Exhaustive
import io.kotest.property.arbitrary.*
import io.kotest.property.exhaustive.azstring
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object RecordSetArbitrator {

    private val validCanons = listOf(
        "AG",
        "AI",
        "AR",
        "BS",
        "BL",
        "BE",
        "FR",
        "GE",
        "GL",
        "GR",
        "JU",
        "LU",
        "NE",
        "NW",
        "OW",
        "SH",
        "SZ",
        "SO",
        "SG",
        "TG",
        "TI",
        "UR",
        "VS",
        "VD",
        "ZG",
        "ZH"
    )

    private val validGeoTerms = listOf("kommunal", "kantonal", "ausserkantonal", "national", "international")

    private val validTimePeriods =
        listOf("Vor 1900", "1900-1919", "1920-1939", "1940-1959", "1960-1979", "1980-1999", "2000-2019", "Nach 2019")

    private val validSubjectTerms = listOf(
        "Alltag / Freizeit",
        "Arbeit",
        "Architektur",
        "Feste",
        "Gastronomie / Hotellerie",
        "Industrie / Technik",
        "Kunst",
        "Landschaft",
        "Landwirtschaft / Alpwirtschaft",
        "Lebensmittel",
        "Naturereignis",
        "Ortsbild",
        "Personen / Portrait",
        "Politik",
        "Reisen",
        "Religion",
        "Sport",
        "Tier- und Pflanzenwelt",
        "Traditionen / Brauchtum",
        "Verkehrsmittel"
    )

    private fun getMediaCollectionArb(type: String): Arb<MediaCollection> {
        return arbitrary {
            MediaCollection(
                type = type,
                field_digital_number = Arb.positiveInt(20000).bind(),
                field_digitized = Arb.positiveInt(100).bind(),
                field_physical_number = Arb.positiveInt(20000).bind(),
                field_indexed = Arb.positiveInt(100).bind(),
                field_geo = Arb.list(Arb.element(validGeoTerms), IntRange(0, 10)).bind().toSet().toList(),
                field_subjects = Arb.list(Arb.element(validSubjectTerms), IntRange(0, 20)).bind().toSet().toList(),
                field_time = Arb.list(Arb.element(validTimePeriods), IntRange(0, 10)).bind().toSet().toList(),
            )
        }
    }

    private val richText: Arb<RichText?> = arbitrary {
        RichText(
            Arb.string(10, 30, Codepoint.alphanumeric()).bind(), null, ""
        )
    }

    private val link = arbitrary {
        Link(
            uri = Arb.bind(
                Arb.domain(labelArb = Arb.string(2, 10, Codepoint.alphanumeric())),
                Arb.stringPattern("[a-z]{1,10}/[a-z]{1,10}")
            ) { x, y -> "$x/$y" }.bind(), title = Arb.string(5, 10).bind(), options = emptyList()
        )
    }

    private val addressArb = arbitrary {
        Address(
            langcode = "",
            country_code = "",
            administrative_area = Arb.element(validCanons).bind(),
            locality = Arb.string(5, 10, codepoints = Codepoint.alphanumeric()).bind(),
            dependent_locality = null,
            postal_code = Arb.int(1000, 9999).bind().toString(),
            sorting_code = null,
            address_line1 = Arb.string(10, 20, Codepoint.alphanumeric()).bind(),
            address_line2 = "",
            organization = "",
            given_name = null,
            additional_name = null,
            family_name = null,
            coordinates = "7.5812, 47.5594",
            main = Arb.boolean().bind()
        )
    }

    val minimalRecordSet = arbitrary {
        RecordSet(
            status = it.random.nextBoolean(),
            field_memobase_id = Arb.stringPattern("[a-z]{3}-\\d{3}").bind(),
            title_de = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            title_fr = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            title_it = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            field_supported_by_memoriav = Arb.boolean().bind(),
            field_institution = Arb.list(Arb.stringPattern("[a-z]{3}"), IntRange(1, 1)).bind()
        )
    }
    val completeRecordSet = arbitrary {
        RecordSet(
            status = it.random.nextBoolean(),
            field_memobase_id = Arb.stringPattern("[a-z]{3}-\\d{3}").bind(),
            title_de = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            title_fr = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            title_it = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            field_supported_by_memoriav = Arb.boolean().bind(),
            field_institution = Arb.list(Arb.stringPattern("[a-z]{3}"), IntRange(1, 1)).bind(),
            field_metadata_language_codes = Arb.list(Arb.element(listOf("de", "fr", "it")), IntRange(0, 10) ).bind(),
            field_processed_teaser_text_de = richText.bind(),
            field_processed_teaser_text_fr = richText.bind(),
            field_processed_teaser_text_it = richText.bind(),
            field_documents_de = Arb.list(link, IntRange(0, 5)).bind(),
            field_documents_fr = Arb.list(link, IntRange(0, 5)).bind(),
            field_documents_it = Arb.list(link, IntRange(0, 5)).bind(),
            field_old_memobase_id = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            field_access_de = richText.bind(),
            field_access_fr = richText.bind(),
            field_access_it = richText.bind(),
            computed_teaser_image_url = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            field_access_memobase_de = richText.bind(),
            field_access_memobase_fr = richText.bind(),
            field_access_memobase_it = richText.bind(),
            field_content_de = richText.bind(),
            field_content_fr = richText.bind(),
            field_content_it = richText.bind(),
            field_context_de = richText.bind(),
            field_context_fr = richText.bind(),
            field_context_it = richText.bind(),
            field_data_transfer_de = richText.bind(),
            field_data_transfer_fr = richText.bind(),
            field_data_transfer_it = richText.bind(),
            field_info_on_development_de = richText.bind(),
            field_info_on_development_fr = richText.bind(),
            field_info_on_development_it = richText.bind(),
            field_language_de = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            field_language_fr = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            field_language_it = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            field_original_description_de = link.bind(),
            field_original_description_fr = link.bind(),
            field_original_description_it = link.bind(),
            field_original_id = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            field_original_shelf_mark = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            field_original_title_de = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            field_original_title_fr = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            field_original_title_it = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            field_project_de = Arb.list(link, IntRange(0, 10)).bind(),
            field_project_fr = Arb.list(link, IntRange(0, 10)).bind(),
            field_project_it = Arb.list(link, IntRange(0, 10)).bind(),
            field_publications_de = Arb.list(link, IntRange(0, 10)).bind(),
            field_publications_fr = Arb.list(link, IntRange(0, 10)).bind(),
            field_publications_it = Arb.list(link, IntRange(0, 10)).bind(),
            field_related_record_sets_de = Arb.list(link, IntRange(0, 10)).bind(),
            field_related_record_sets_fr = Arb.list(link, IntRange(0, 10)).bind(),
            field_related_record_sets_it = Arb.list(link, IntRange(0, 10)).bind(),
            field_resp_institution_access = Arb.list(Arb.stringPattern("[a-z]{3}"), IntRange(0, 10)).bind(),
            field_resp_institution_master = Arb.list(Arb.stringPattern("[a-z]{3}"), IntRange(0, 10)).bind(),
            field_resp_institution_original = Arb.list(Arb.stringPattern("[a-z]{3}"), IntRange(0, 10)).bind(),
            field_rights_de = richText.bind(),
            field_rights_fr = richText.bind(),
            field_rights_it = richText.bind(),
            field_scope_de = richText.bind(),
            field_scope_fr = richText.bind(),
            field_scope_it = richText.bind(),
            field_selection_de = richText.bind(),
            field_selection_fr = richText.bind(),
            field_selection_it = richText.bind(),
            field_time_period = Arb.stringPattern("\\d{4}/\\d{4}").bind(),
            field_transfer_date = Arb.localDate().bind().format(DateTimeFormatter.ISO_DATE)
        )
    }
}