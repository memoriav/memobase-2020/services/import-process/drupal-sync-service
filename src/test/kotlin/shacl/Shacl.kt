/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.shacl

import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory

object Shacl {

    /**
     * Superclass of NodeShape and PropertyShape.
     */
    val Shape = res("Shape")

    /**
     * This is the rdf:type of any shape that describes a focus node.
     */
    val NodeShape = res("NodeShape")

    /**
     * A node target is specified using the sh:targetNode predicate. Each value of sh:targetNode in a shape is
     * either an IRI or a literal.
     */
    val targetNode = prop("targertNode")

    /**
     * A class target is specified with the sh:targetClass predicate. Each value of sh:targetClass in a shape is an IRI.
     */
    val targetClass = prop("targetClass")

    /**
     *  A subjects-of target is specified with the predicate sh:targetSubjectsOf. The values of sh:targetSubjectsOf
     *  in a shape are IRIs.
     */
    val targetSubjectsOf = prop("targetSubjectsOf")

    /**
     *  An objects-of target is specified with the predicate sh:targetObjectsOf. The values of sh:targetObjectsOf
     *  in a shape are IRIs.
     */
    val targetObjectsOf = prop("targetObjectsOf")

    /**
     *  Shapes can specify one value for the property sh:severity in the shapes graph. Each value of sh:severity in
     *  a shape is an IRI. The default
     *  is always Violation. Which is the only Severity that causes the target to not conform.
     */
    val severity = prop("severity")
    /**
     * A non-critical constraint violation indicating an informative message.
     */
    val Info = res("Info")
    /**
     * A non-critical constraint violation indicating a warning.
     */
    val Warning = res("Warning")
    /**
     * A constraint violation. This is the default.
     */
    val Violation = res("Violation")

    /**
     * Shapes can have values for the property sh:message. The values of sh:message in a shape are either
     * xsd:string literals or literals with a language tag. A shape should not have more than one value for
     * sh:message with the same language tag.
     */
    val message = prop("message")

    /**
     *  Shapes can have at most one value for the property sh:deactivated. The value of sh:deactivated in a
     *  shape must be either true or false.
     */
    val deactivated = prop("deactivated")

    /**
     *   Property shapes may have one or more values for sh:name to provide human-readable labels for the
     *   property in the target where it appears. If present, tools SHOULD prefer those locally specified
     *   labels over globally specified labels at the rdf:Property itself. For example, if a form displays
     *   a node that is in the target of a given property shape with an sh:name, then the tool SHOULD use
     *   the provided name. Similarly, property shape may have values for sh:description to provide descriptions
     *   of the property in the given context. Both sh:name and sh:description may have multiple values,
     *   but should only have one value per language tag.
     */
    val name = prop("name")

    /**
     *   Property shapes may have one or more values for sh:name to provide human-readable labels for the
     *   property in the target where it appears. If present, tools SHOULD prefer those locally specified
     *   labels over globally specified labels at the rdf:Property itself. For example, if a form displays
     *   a node that is in the target of a given property shape with an sh:name, then the tool SHOULD use
     *   the provided name. Similarly, property shape may have values for sh:description to provide descriptions
     *   of the property in the given context. Both sh:name and sh:description may have multiple values,
     *   but should only have one value per language tag.
     */
    val description = prop("description")

    /**
     *   Property shapes may have one value for the property sh:order to indicate the relative order of the property
     *   shape for purposes such as form building. The values of sh:order are decimals. sh:order is not used for
     *   validation purposes and may be used with any type of subjects. If present at property shapes, the recommended
     *   use of sh:order is to sort the property shapes in an ascending order, for example so that properties with
     *   smaller order are placed above (or to the left) of properties with larger order.
     */
    val order = prop("order")

    /**
     *    Property shapes may link to an SHACL instance of the class sh:PropertyGroup using
     *    the property sh:group to indicate that the shape belongs to a group of related
     *    property shapes. Each group may have additional triples that serve application
     *    purposes, such as an rdfs:label for form building. Groups may also have an sh:order
     *    property to indicate the relative ordering of groups within the same form.
     */
    val group = prop("group")

    /**
     *    Property shapes may have a single value for sh:defaultValue. The default value does not have fixed
     *    semantics in SHACL, but MAY be used by user interface tools to pre-populate input widgets.
     *    The value type of the sh:defaultValue should align with the specified sh:datatype or
     *    sh:class of the same shape.
     */
    val defaultValue = prop("defaultValue")

    /**
     * This is the rdf:type of any shape that describes properties of the focus node.
     */
    val PropertyShape = res("PropertyShape")

    /**
     * The predicate that the PropertyShape applies to.
     */
    val path = prop("path")

    /**
     *    The type of all value nodes. The values of sh:class in a shape are IRIs.
     */
    val shClass = prop("class")

    /**
     *   The datatype of all value nodes (e.g., xsd:integer). The values of sh:datatype in a shape are IRIs.
     *   A shape has at most one value for sh:datatype.
     */
    val datatype = prop("datatype")

    /**
     *   The node kind (IRI, blank node, literal or combinations of these) of all value nodes.
     *   The values of sh:nodeKind in a shape are one of the following six instances of the class
     *   sh:NodeKind: sh:BlankNode, sh:IRI, sh:Literal sh:BlankNodeOrIRI, sh:BlankNodeOrLiteral and sh:IRIOrLiteral.
     *   A shape has at most one value for sh:nodeKind.
     */
    val nodeKind = prop("nodeKind")

    val NodeKind = res("NodeKind")
    val BlankNode = res("BlankNode")
    val IRI = res("IRI")
    val Literal = res("Literal")
    val BlankNodeOrIRI = res("BlankNodeOrIRI")
    val BlankNodeOrLiteral = res("BlankNodeOrLiteral")
    val IRIOrLiteral = res("IRIOrLiteral")

    /**
     *   The minimum cardinality. Node shapes cannot have any value for sh:minCount. A property shape
     *   has at most one value for sh:minCount. The values of sh:minCount in a property shape are literals
     *   with datatype xsd:integer.
     */
    val minCount = prop("minCount")

    /**
     *   The maximum cardinality. Node shapes cannot have any value for sh:maxCount. A property shape
     *   has at most one value for sh:maxCount. The values of sh:maxCount in a property shape are
     *   literals with datatype xsd:integer.
     */
    val maxCount = prop("maxCount")

    /**
     *    	The minimum exclusive value. The values of sh:minExclusive in a shape are literals.
     *    	A shape has at most one value for sh:minExclusive.
     */
    val minExclusive = prop("minExclusive")

    /**
     *  The minimum inclusive value. The values of sh:minInclusive in a shape are literals.
     *    	A shape has at most one value for sh:minInclusive.
     */
    val minInclusive = prop("minInclusive")

    /**
     *   The maximum exclusive value. The values of sh:maxExclusive in a shape are literals.
     *   A shape has at most one value for sh:maxExclusive.
     */
    val maxExclusive = prop("maxExclusive")

    /**
     *   	The maximum inclusive value. The values of sh:maxInclusive in a shape are literals.
     *   	A shape has at most one value for sh:maxInclusive.
     */
    val maxInclusive = prop("maxInclusive")

    /**
     *  The minimum length. The values of sh:minLength in a shape are literals with datatype xsd:integer.
     *  A shape has at most one value for sh:minLength.
     */
    val minLength = prop("minLength")

    /**
     *  The maximum length. The values of sh:maxLength in a shape are literals with datatype xsd:integer.
     *  A shape has at most one value for sh:maxLength.
     */
    val maxLength = prop("maxLength")

    /**
     * n 	A regular expression that all value nodes need to match. The values of sh:pattern in a shape are
     * literals with datatype xsd:string. The values of sh:pattern in a shape are
     * valid pattern arguments for the SPARQL REGEX function.
     */
    val pattern = prop("pattern")

    /**
     *  An optional string of flags, interpreted as in SPARQL 1.1 REGEX. The values of
     *  sh:flags in a shape are literals with datatype xsd:string.
     */
    val flags = prop("flags")

    /**
     *   	A list of basic language ranges as per [BCP47]. Each value of sh:languageIn in a shape is a SHACL list.
     *   	Each member of such a list is a literal with datatype xsd:string. A shape
     *   	has at most one value for sh:languageIn.
     */
    val languageIn = prop("languageIn")

    /**
     *   	 	true to activate this constraint. The values of sh:uniqueLang in a shape are literals with datatype
     *   	 	xsd:boolean. A property shape has at most one value for sh:uniqueLang.
     *   	 	Node shapes cannot have any value for sh:uniqueLang.
     */
    val uniqueLang = prop("uniqueLang")

    /**
     * The property to compare with. The values of sh:equals in a shape are IRIs.
     */
    val equals = prop("equals")

    /**
     * The property to compare the values with. The values of sh:disjoint in a shape are IRIs.
     */
    val disjoint = prop("disjoint")

    /**
     * The property to compare the values with. The values of sh:lessThan in a shape are IRIs.
     * Node shapes cannot have any value for sh:lessThan.
     */
    val lessThan = prop("lessThan")

    /**
     * The property to compare the values with. The values of sh:lessThanOrEquals in a
     * shape are IRIs. Node shapes cannot have any value for sh:lessThanOrEquals.
     */
    val lessThanOrEquals = prop("lessThanOrEquals")

    /**
     *  The shape to negate. The values of sh:not in a shape must be well-formed shapes.
     */
    val not = prop("not")

    /**
     *  A SHACL list of shapes to validate the value nodes against. Each value of sh:and in a
     *  shape is a SHACL list. Each member of such list must be a well-formed shape.
     */
    val and = prop("and")

    /**
     *  A SHACL list of shapes to validate the value nodes against. Each value of sh:or in a shape is a
     *  SHACL list. Each member of such list must be a well-formed shape.
     */
    val or = prop("or")

    /**
     *  A SHACL list of shapes to validate the value nodes against. Each value of sh:xone in a shape is a
     *  SHACL list. Each member of such list must be a well-formed shape.
     */
    val xone = prop("xone")

    /**
     *  The node shape that all value nodes need to conform to. The values of sh:node in a
     *  shape must be well-formed node shapes.
     */
    val node = prop("node")

    /**
     * A property shape that all value nodes need to have. Each value of sh:property in a shape
     * must be a well-formed property shape.
     */
    val property = prop("property")

    /**
     * The shape that the specified number of value nodes needs to conform to. The values of sh:qualifiedValueShape
     * in a shape must be well-formed shapes. Node shapes cannot have any value for sh:qualifiedValueShape.
     * This is a mandatory parameter of sh:QualifiedMinCountConstraintComponent and
     * sh:QualifiedMaxCountConstraintComponent.
     */
    val qualifiedValueShape = prop("qualifiedValueShape")

    /**
     * This is an optional parameter of sh:QualifiedMinCountConstraintComponent and
     * sh:QualifiedMaxCountConstraintComponent. If set to true then (for the counting) the value nodes must
     * not conform to any of the sibling shapes. The values of sh:qualifiedValueShapesDisjoint
     * in a shape are literals with datatype xsd:boolean.
     */
    val qualifiedValueShapesDisjoint = prop("qualifiedValueShapesDisjoint")

    /**
     * The minimum number of value nodes that conform to the shape. The values of sh:qualifiedMinCount in a shape
     * are literals with datatype xsd:integer. This is a mandatory parameter of
     * sh:QualifiedMinCountConstraintComponent.
     */
    val qualifiedMinCount = prop("qualifiedMinCount")

    /**
     * The maximum number of value nodes that can conform to the shape. The values of sh:qualifiedMaxCount in a
     * shape are literals with datatype xsd:integer. This is a mandatory parameter of
     * sh:QualifiedMaxCountConstraintComponent.
     */
    val qualifiedMaxCount = prop("qualifiedMaxCount")

    /**
     * Set to true to close the shape. The values of sh:closed in a shape are literals with datatype xsd:boolean.
     */
    val closed = prop("closed")

    /**
     * Optional SHACL list of properties that are also permitted in addition to those explicitly enumerated via
     * sh:property. The values of sh:ignoredProperties in a shape must be SHACL lists.
     * Each member of such a list must be a IRI.
     */
    val ignoredProperties = prop("ignoredProperties")

    /**
     * A specific required value.
     */
    val hasValue = prop("hasValue")

    /**
     * A SHACL list that has the allowed values as members. Each value of sh:in in a
     * shape is a SHACL list. A shape has at most one value for sh:in.
     */
    val shIn = prop("in")


    private fun prop(name: String): Property {
        return ResourceFactory.createProperty(NS.shacl, name)
    }

    private fun res(name: String): Resource {
        return ResourceFactory.createResource(NS.shacl + name)
    }
}