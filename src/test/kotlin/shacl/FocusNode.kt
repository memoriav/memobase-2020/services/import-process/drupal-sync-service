/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.shacl

import ch.memobase.rdf.RDF
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.RDFNode

class FocusNode(private val model: Model, uri: String) {
    
    private val focusNode = model.createResource(uri)

    init {
        focusNode.addProperty(RDF.type, Shacl.NodeShape)
        focusNode.addLiteral(Shacl.closed, true)
        focusNode.addProperty(Shacl.ignoredProperties, model.createList(RDF.type))
    }

    fun addProperty(path: Property): PropertyNode {
        return PropertyNode(model, focusNode, path)
    }

    fun isNodeKind(): FocusNode {
        focusNode.addProperty(Shacl.nodeKind, Shacl.NodeKind)
        return this
    }

    fun isIRINodeKind(): FocusNode {
        focusNode.addProperty(Shacl.nodeKind, Shacl.IRI)
        return this
    }

    fun isBlankNodeKind(): FocusNode {
        focusNode.addProperty(Shacl.nodeKind, Shacl.BlankNode)
        return this
    }

    fun isLiteralNodeKind(): FocusNode {
        focusNode.addProperty(Shacl.nodeKind, Shacl.Literal)
        return this
    }

    fun isIRIOrLiteralNodeKind(): FocusNode {
        focusNode.addProperty(Shacl.nodeKind, Shacl.IRIOrLiteral)
        return this
    }

    fun isBlankNodeOrIRINodeKind(): FocusNode {
        focusNode.addProperty(Shacl.nodeKind, Shacl.BlankNodeOrIRI)
        return this
    }

    fun isBlankNodeOrLiteralKind(): FocusNode {
        focusNode.addProperty(Shacl.nodeKind, Shacl.BlankNodeOrLiteral)
        return this
    }

    fun setTargetNode(target: RDFNode): FocusNode {
        focusNode.addProperty(Shacl.targetNode, target)
        return this
    }

    fun setTargetClass(target: RDFNode): FocusNode {
        focusNode.addProperty(Shacl.targetClass, target)
        return this
    }

    fun setTargetObjectOf(target: RDFNode): FocusNode {
        focusNode.addProperty(Shacl.targetObjectsOf, target)
        return this
    }

    fun setTargetSubjectOf(target: RDFNode): FocusNode {
        focusNode.addProperty(Shacl.targetSubjectsOf, target)
        return this
    }
}