/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.shacl

import ch.memobase.rdf.RDF
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource

class PropertyNode(private val model: Model, subject: Resource, path: Property) {

    private val propertyNode = model.createResource()

    init {
        propertyNode.addProperty(RDF.type, Shacl.PropertyShape)
        propertyNode.addProperty(Shacl.path, path)
        subject.addProperty(Shacl.property, propertyNode)
    }

    fun isNodeKind(): PropertyNode {
        propertyNode.addProperty(Shacl.nodeKind, Shacl.NodeKind)
        return this
    }

    fun isIRINodeKind(): PropertyNode {
        propertyNode.addProperty(Shacl.nodeKind, Shacl.IRI)
        return this
    }

    fun isBlankNodeKind(): PropertyNode {
        propertyNode.addProperty(Shacl.nodeKind, Shacl.BlankNode)
        return this
    }

    fun isLiteralNodeKind(datatype: Property): PropertyNode {
        propertyNode.addProperty(Shacl.nodeKind, Shacl.Literal)
        propertyNode.addProperty(Shacl.datatype, datatype)
        return this
    }

    fun isIRIOrLiteralNodeKind(): PropertyNode {
        propertyNode.addProperty(Shacl.nodeKind, Shacl.IRIOrLiteral)
        return this
    }

    fun isBlankNodeOrIRINodeKind(): PropertyNode {
        propertyNode.addProperty(Shacl.nodeKind, Shacl.BlankNodeOrIRI)
        return this
    }

    fun isBlankNodeOrLiteralKind(): PropertyNode {
        propertyNode.addProperty(Shacl.nodeKind, Shacl.BlankNodeOrLiteral)
        return this
    }

    fun minCountCardinality(value: Int): PropertyNode {
        propertyNode.addLiteral(Shacl.minCount, model.createTypedLiteral(value, XSD.integer.uri))
        return this
    }

    fun maxCountCardinality(value: Int): PropertyNode {
        propertyNode.addLiteral(Shacl.maxCount, model.createTypedLiteral(value, XSD.integer.uri))
        return this
    }

    fun pattern(value: String): PropertyNode {
        propertyNode.addLiteral(Shacl.pattern, value)
        return this
    }


}