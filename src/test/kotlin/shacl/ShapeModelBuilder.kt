/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.shacl

import ch.memobase.rdf.MB
import ch.memobase.rdf.RICO
import ch.memobase.rdf.WD


fun main() {
    ShapeModelBuilder.completeInstitutionTestShapeModel()
}

object ShapeModelBuilder {

    fun completeInstitutionTestShapeModel() {
        val model = ShapeModel()
        model.prefixName = "CompleteInstitutionTest"

        val focusNode = model.addFocusNode(RICO.CorporateBody.localName)
            .isIRINodeKind()
            .setTargetNode(RICO.CorporateBody)

        focusNode
            .addProperty(RICO.isOrWasHolderOf)
            .isIRINodeKind()
            .minCountCardinality(1)
            .maxCountCardinality(10)
            .pattern("[a-z]{3}")

        focusNode
            .addProperty(RICO.hasOrHadLocation)
            .isIRINodeKind()
            .minCountCardinality(1)
            .maxCountCardinality(5)

        focusNode
            .addProperty(RICO.hasOrHadIdentifier)
            .isBlankNodeKind()
            .minCountCardinality(1)
            .maxCountCardinality(2)

        focusNode
            .addProperty(MB.isPublished)
            .isLiteralNodeKind(XSD.boolean)
            .minCountCardinality(1)
            .maxCountCardinality(1)

        focusNode
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .minCountCardinality(0)
            .maxCountCardinality(2)
            .pattern("memobaseInstitution|surveyInstitution")

        focusNode
            .addProperty(WD.typeOfInstitution)
            .minCountCardinality(1)
            .maxCountCardinality(10)
            .pattern("http://www.wikidata.org/entity/Q\\\\d{1,10}")



        model.writeModelToFile("src/test/resources/data/institution/complete", "new")
    }
}