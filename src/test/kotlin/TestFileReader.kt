/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestFileReader {
    private val labelResourcePath = "src/test/resources/labels"

    @Test
    fun `test municipalities loader`() {
        val result = Util.readLabelFile("$labelResourcePath/municipalities.csv")
        assertAll(
            "",
            {
                assertThat(result)
                    .isNotEmpty
            },
            {
                assertThat(result["8506"]?.id)
                    .isEqualTo("http://www.wikidata.org/entity/Q65761")

            },
            {
                assertThat(result["8535"]?.id)
                    .isEqualTo("http://www.wikidata.org/entity/Q65761")

            }
        )
    }

    @Test
    fun `test cantons loader`() {
        val result = Util.readLabelFile("$labelResourcePath/cantons.csv")
        assertAll(
            "",
            {
                assertThat(result)
                    .hasSize(26)
            }
        )
    }

    @Test
    fun `test metadata languages loader`() {
        val result = Util.readLabelFile("$labelResourcePath/metadata-languages.csv")

        assertAll(
            "",
            {
                assertThat(result)
                    .hasSize(3)
            }
        )

    }
}