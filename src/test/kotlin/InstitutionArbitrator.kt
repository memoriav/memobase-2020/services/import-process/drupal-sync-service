/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.model.*
import io.kotest.property.Arb
import io.kotest.property.Exhaustive
import io.kotest.property.arbitrary.*
import io.kotest.property.exhaustive.azstring
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object InstitutionArbitrator {
    const val dataPath = "src/test/resources/data"


    private val validCanons = listOf(
        "AG",
        "AI",
        "AR",
        "BS",
        "BL",
        "BE",
        "FR",
        "GE",
        "GL",
        "GR",
        "JU",
        "LU",
        "NE",
        "NW",
        "OW",
        "SH",
        "SZ",
        "SO",
        "SG",
        "TG",
        "TI",
        "UR",
        "VS",
        "VD",
        "ZG",
        "ZH"
    )

    private val validGeoTerms = listOf("kommunal", "kantonal", "ausserkantonal", "national", "international")

    private val validTimePeriods =
        listOf("Vor 1900", "1900-1919", "1920-1939", "1940-1959", "1960-1979", "1980-1999", "2000-2019", "Nach 2019")

    private val validSubjectTerms = listOf(
        "Alltag / Freizeit",
        "Arbeit",
        "Architektur",
        "Feste",
        "Gastronomie / Hotellerie",
        "Industrie / Technik",
        "Kunst",
        "Landschaft",
        "Landwirtschaft / Alpwirtschaft",
        "Lebensmittel",
        "Naturereignis",
        "Ortsbild",
        "Personen / Portrait",
        "Politik",
        "Reisen",
        "Religion",
        "Sport",
        "Tier- und Pflanzenwelt",
        "Traditionen / Brauchtum",
        "Verkehrsmittel"
    )

    private fun getMediaCollectionArb(type: String): Arb<MediaCollection> {
        return arbitrary {
            MediaCollection(
                type = type,
                field_digital_number = Arb.positiveInt(20000).bind(),
                field_digitized = Arb.positiveInt(100).bind(),
                field_physical_number = Arb.positiveInt(20000).bind(),
                field_indexed = Arb.positiveInt(100).bind(),
                field_geo = Arb.list(Arb.element(validGeoTerms), IntRange(0, 10)).bind().toSet().toList(),
                field_subjects = Arb.list(Arb.element(validSubjectTerms), IntRange(0, 20)).bind().toSet().toList(),
                field_time = Arb.list(Arb.element(validTimePeriods), IntRange(0, 10)).bind().toSet().toList(),
            )
        }
    }

    private val addressArb = arbitrary {
        Address(
            langcode = "",
            country_code = "",
            administrative_area = Arb.element(validCanons).bind(),
            locality = Arb.string(5, 10, codepoints = Codepoint.alphanumeric()).bind(),
            dependent_locality = null,
            postal_code = Arb.int(1000, 9999).bind().toString(),
            sorting_code = null,
            address_line1 = Arb.string(10, 20, Codepoint.alphanumeric()).bind(),
            address_line2 = "",
            organization = "",
            given_name = null,
            additional_name = null,
            family_name = null,
            coordinates = "7.5812, 47.5594",
            main = Arb.boolean().bind()
        )
    }

    val minimalInstitution = arbitrary {
        Institution(
            status = it.random.nextBoolean(),
            field_memobase_id = Exhaustive.azstring(IntRange(3, 3)).toArb().bind(),
            field_wikidata_id = "http://www.wikidata.org/wiki/Q${Arb.int(1, 100000).bind()}",
            field_master_institution_types = Arb.list(
                Arb.stringPattern("http://www[.]wikidata[.]org/wiki/Q${it.random.nextInt(20, 10000)}"), IntRange(1, 10)
            ).bind(),
            title_de = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            title_fr = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            title_it = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            recordset_ids = Arb.list(
                Arb.stringPattern("[a-z]{3}-[0-9]{3}"), IntRange(1, 10)
            ).bind(),
            field_address = Arb.list(
                addressArb, IntRange(1, 5)
            ).bind()
        )
    }


    val completeInstitution = arbitrary {
        Institution(
            status = Arb.boolean().bind(),
            field_memobase_id = Exhaustive.azstring(IntRange(3, 3)).toArb().bind(),
            field_wikidata_id = "http://www.wikidata.org/wiki/Q${Arb.int(1, 100000).bind()}",
            field_master_institution_types = Arb.list(
                Arb.stringPattern("http://www[.]wikidata[.]org/wiki/Q${it.random.nextInt(20, 10000)}"), IntRange(1, 10)
            ).bind(),
            title_de = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            title_fr = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            title_it = Arb.string(1, 50, Codepoint.alphanumeric()).bind(),
            recordset_ids = Arb.list(
                Arb.stringPattern("[a-z]{3}-[0-9]{3}"), IntRange(1, 2)
            ).bind(),
            field_address = Arb.list(
                addressArb, IntRange(1, 2)
            ).bind(),
            field_memobase_institution = Arb.boolean().bind(),
            // this is always true in order to ensure that the collections are always generated.
            // The minimal approach tests what happens when this is always false.
            field_survey_institution = true,
            field_private = Arb.boolean().bind(),
            field_isil = Arb.string(5, 16, Codepoint.alphanumeric()).bind(),
            field_old_memobase_id = Arb.string(3, 20, Codepoint.alphanumeric()).bind(),
            field_email = Arb.email(
                localPartGen = Arb.string(2, 10, Codepoint.alphanumeric()),
                domainGen = Arb.domain(labelArb = Arb.string(2, 10, Codepoint.alphanumeric()))
            )
                .bind(),
            field_website = Link(
                uri = Arb.bind(
                    Arb.domain(labelArb = Arb.string(2, 10, Codepoint.alphanumeric())),
                    Arb.stringPattern("[a-z]{1,10}/[a-z]{1,10}")
                ) { x, y -> "$x/$y" }.bind(), title = Arb.string(5, 10).bind(), options = emptyList()
            ),
            field_link_archive_catalog = Link(
                uri = Arb.bind(
                    Arb.domain(labelArb = Arb.string(2, 10, Codepoint.alphanumeric())),
                    Arb.stringPattern("[a-z]{1,10}/[a-z]{1,10}")
                ) { x, y -> "$x/$y" }.bind(), title = Arb.string(5, 10).bind(), options = emptyList()
            ),
            field_text_de = RichText(
                Arb.string(10, 30, Codepoint.alphanumeric()).bind(), null, ""
            ),
            field_text_fr = RichText(
                Arb.string(10, 30, Codepoint.alphanumeric()).bind(), null, ""
            ),
            field_text_it = RichText(
                Arb.string(10, 30, Codepoint.alphanumeric()).bind(), null, ""
            ),
            computed_teaser_image_url = Arb.string(10, 20, Codepoint.alphanumeric()).bind(),
            field_teaser_color = Arb.stringPattern("#\\d{6}").bind(),
            field_accessibility_de = Arb.string(50, 100, Codepoint.alphanumeric()).bind(),
            field_accessibility_fr = Arb.string(50, 100, Codepoint.alphanumeric()).bind(),
            field_accessibility_it = Arb.string(50, 100, Codepoint.alphanumeric()).bind(),
            field_survey_date = Arb.zonedDateTime(
                minValue = LocalDateTime.of(2010, 1, 1, 1, 1), maxValue = LocalDateTime.of(2040, 1, 1, 1, 1)
            ).bind().format(DateTimeFormatter.ISO_DATE_TIME),
            collections = AvkCollections(
                Film = getMediaCollectionArb("Film").bind(),
                Foto = getMediaCollectionArb("Foto").bind(),
                Andere = getMediaCollectionArb("Andere").bind(),
                Ton = getMediaCollectionArb("Ton").bind(),
                Video = getMediaCollectionArb("Video").bind()
            )
        )
    }
}