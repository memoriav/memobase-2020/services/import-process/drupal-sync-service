/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.reporting.ReportStatus
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File
import java.nio.charset.Charset

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestJsonParser {


    private val resourcePath = "src/test/resources/data/json"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName.json").readText(Charset.defaultCharset())
    }

    @Test
    fun `test institution json parser`() {
        val result = JSON.parseJson("test", readFile("test"), "test")
        val report = result.second
        assertAll("assert institution output",
            {
                assertThat(report.status)
                    .isEqualTo(ReportStatus.success)
            },
            {
                assertThat(report.message)
                    .isEqualTo("Successfully parsed json.")
            }

        )
    }
}