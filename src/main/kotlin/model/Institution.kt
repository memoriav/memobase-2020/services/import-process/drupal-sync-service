/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.model

import ch.memobase.Util
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("node--institution")
data class Institution(
    // mandatory fields
    val status: Boolean,
    val field_memobase_id: String,
    val field_wikidata_id: String?,
    val field_master_institution_types: List<String>,
    val field_other_institution_types: List<String>? = null,

    val title_de: String,
    val title_fr: String,
    val title_it: String,

    val recordset_ids: List<String>,
    val field_address: List<Address?>,

    // optional fields
    val field_memobase_institution: Boolean? = null,
    val field_survey_institution: Boolean? = null,
    val field_private: Boolean? = null,

    val field_isil: String? = null,
    val field_old_memobase_id: String? = null,
    val field_email: String? = null,

    val field_website: Link? = null,
    val field_link_archive_catalog: Link? = null,

    val field_text_de: RichText? = null,
    val field_text_fr: RichText? = null,
    val field_text_it: RichText? = null,

    val computed_teaser_image_url: String? = null,
    val field_teaser_color: String? = null,
    val field_accessibility_de: String? = null,
    val field_accessibility_fr: String? = null,
    val field_accessibility_it: String? = null,
    val computed_teaser_color: String? = null,
    val field_survey_date: String? = null,
    val collections: AvkCollections? = null
) : Input(Util.drupalInstitutionTypeIdentifier)