/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.model

import ch.memobase.Util
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("node--record_set")
data class RecordSet(
    /* Publikations Status */
    val status: Boolean,
    /* Teaser text for search result. Not reflected in RDF Model. */
    val field_processed_teaser_text_de: RichText? = null,
    val field_processed_teaser_text_fr: RichText? = null,
    val field_processed_teaser_text_it: RichText? = null,
    /* 0.1 Titel (Memobase) */
    val title_de: String,
    val title_fr: String,
    val title_it: String,
    /* 0.2.1 Thumbnail Searchresult */
    val computed_teaser_image_url: String? = null,
    /* 1.1 Inhalt */
    val field_content_de: RichText? = null,
    val field_content_fr: RichText? = null,
    val field_content_it: RichText? = null,
    /* 1.2 Entstehungszeitrum */
    val field_time_period: String? = null,
    /// 1.3 Sprache
    val field_language_de: String? = null,
    val field_language_fr: String? = null,
    val field_language_it: String? = null,
    /* 1.4 Zugang Memobase */
    val field_access_memobase_de: RichText? = null,
    val field_access_memobase_fr: RichText? = null,
    val field_access_memobase_it: RichText? = null,
    /* 2.1 Kontext */
    val field_context_de: RichText? = null,
    val field_context_fr: RichText? = null,
    val field_context_it: RichText? = null,
    /* 3.1 Titel (original) */
    val field_original_title_de: String? = null,
    val field_original_title_fr: String? = null,
    val field_original_title_it: String? = null,
    /* 3.2 Umfang */
    val field_scope_de: RichText? = null,
    val field_scope_fr: RichText? = null,
    val field_scope_it: RichText? = null,
    /* 3.3 Auswahl */
    val field_selection_de: RichText? = null,
    val field_selection_fr: RichText? = null,
    val field_selection_it: RichText? = null,
    // 3.4 Informationen zur Erschliessung
    val field_info_on_development_de: RichText? = null,
    val field_info_on_development_fr: RichText? = null,
    val field_info_on_development_it: RichText? = null,
    /* 3.5 Sprache Metadaten */
    val field_metadata_language_codes: List<String> = emptyList(),
    /* 3.6 Rechte */
    val field_rights_de: RichText? = null,
    val field_rights_fr: RichText? = null,
    val field_rights_it: RichText? = null,
    /* 3.7 Originaler Identifier */
    val field_original_id: String? = null,
    /* 3.8 Bestandes Signatur */
    val field_original_shelf_mark: String? = null,
    /* 3.9 Original Bestandesbeschreibung (Titel) */
    /* 3.9.1 Original Bestandesbeschreibung (Link) */
    val field_original_description_de: Link? = null,
    val field_original_description_it: Link? = null,
    val field_original_description_fr: Link? = null,
    /* 4.1 Zugang */
    val field_access_de: RichText? = null,
    val field_access_fr: RichText? = null,
    val field_access_it: RichText? = null,
    /* 4.2 Original Institution */
    val field_resp_institution_original: List<String> = emptyList(),
    /* 4.3 Master Institution */
    val field_resp_institution_master: List<String> = emptyList(),
    /* 4.4 Zugangs Institution*/
    val field_resp_institution_access: List<String> = emptyList(),
    /* 5.1 Projektname */
    /* 5.2 Projektbeschreibung (Link) */
    val field_project_de: List<Link> = emptyList(),
    val field_project_fr: List<Link> = emptyList(),
    val field_project_it: List<Link> = emptyList(),
    /* 5.1 Verwandte Bestände (Titel) */
    /* 5.2 Verwandte Bestände (Link) */
    val field_related_record_sets_de: List<Link> = emptyList(),
    val field_related_record_sets_fr: List<Link> = emptyList(),
    val field_related_record_sets_it: List<Link> = emptyList(),
    /* 5.5 Puplikation (Titel) */
    /* 5.6 Publikation (Link) */
    val field_publications_de: List<Link> = emptyList(),
    val field_publications_fr: List<Link> = emptyList(),
    val field_publications_it: List<Link> = emptyList(),
    /* 5.7 Verwandte Dokumente (Titel) */
    /* 5.8 Verwandte Dokumente (Link) */
    val field_documents_de: List<Link> = emptyList(),
    val field_documents_fr: List<Link> = emptyList(),
    val field_documents_it: List<Link> = emptyList(),
    /* 6.1 Datenübernahme */
    val field_data_transfer_de: RichText? = null,
    val field_data_transfer_fr: RichText? = null,
    val field_data_transfer_it: RichText? = null,
    /* 6.2 Datum Übernahme (YYYY-MM-DD) */
    val field_transfer_date: String? = null,
    /* 6.4 Memobase ID (Neu)*/
    val field_memobase_id: String,
    /* 8.1 Unterstützt durch Memoriav */
    val field_supported_by_memoriav: Boolean,
    /* 8.2 Institution */
    val field_institution: List<String>,
    /* 9.3 Memobase ID (Alt)*/
    val field_old_memobase_id: String? = null,
    val field_notes: RichText? = null,
    val field_image_gallery: DataLink? = null,
    val field_teaser_image: DataLink? = null,
) : Input(Util.drupalRecordSetTypeIdentifier)


@Serializable
data class DataLink(
    val data: DataSource? = null,
    val links: ObjectLinks? = null
)

@Serializable
data class ObjectLinks(
    val self: Href? = null,
    val related: Href? = null
)

@Serializable
data class Href(
    val href: String? = null
)

@Serializable
data class DataSource(
    val type: String? = null,
    val id: String? = null,
    val meta: MetaIds? = null,
)

@Serializable
data class MetaIds(
    val target_revision_id: Int? = null,
    val drupal_internal__target_id: Int? = null
)