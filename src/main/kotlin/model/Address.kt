/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.model

import kotlinx.serialization.Serializable

@Serializable
data class Address(
    val langcode: String? = null,
    val country_code: String,
    val administrative_area: String,
    val locality: String,
    val dependent_locality: String? = null,
    val postal_code: String,
    val sorting_code: String? = null,
    val address_line1: String,
    val address_line2: String? = null,
    val organization: String? = null,
    val given_name: String? = null,
    val additional_name: String? = null,
    val family_name: String? = null,
    val coordinates: String,
    val main: Boolean?
)