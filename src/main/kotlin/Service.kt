/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.settings.SettingsLoader
import org.apache.kafka.streams.KafkaStreams
import org.apache.logging.log4j.LogManager

class Service(private val settings: SettingsLoader) {
    private val log = LogManager.getLogger(this::class.java)
    private val topology = KafkaTopology(settings).build()
    private val stream = KafkaStreams(topology, settings.kafkaStreamsSettings)

    fun run() {
        stream.use {
            log.info("Start Kafka Streams Application with id ${settings.kafkaStreamsSettings.getProperty("application.id")}")
            it.start()
            while (stream.state().isRunningOrRebalancing) {
                Thread.sleep(10_000L)
            }
            log.info("Stopping Kafka Streams Application")
        }
    }
}
