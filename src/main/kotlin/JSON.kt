/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.model.Input
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import org.apache.logging.log4j.LogManager

object JSON {
    private val log = LogManager.getLogger(this::class.java)

    @OptIn(ExperimentalSerializationApi::class)
    private val format = Json { explicitNulls = false }

    fun parseJson(key: String, data: String, step: String): Pair<Input?, Report> {
        return try {
            val output = format.decodeFromString<Input>(data)
            Pair(
                output, Report(
                    key,
                    ReportStatus.success,
                    "Successfully parsed json.",
                    step
                )
            )
        } catch (ex: Exception) {
            log.error("Failed to parse $key json: ${ex.message}.")
            Pair(
                null,
                Report(
                    key,
                    ReportStatus.fatal,
                    "Failed to parse json: ${ex.message}.",
                    step
                )
            )
        }
    }
}