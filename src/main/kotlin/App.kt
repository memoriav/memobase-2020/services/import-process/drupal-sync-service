/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import org.apache.logging.log4j.LogManager
import ch.memobase.Util.loadSettings
import kotlin.system.exitProcess

class App {
    companion object {
        private val log = LogManager.getLogger(this::class.java)

        @JvmStatic
        fun main(args: Array<String>) {
            val settings = try {
                loadSettings("app.yml")
            } catch (ex: Exception) {
                ex.printStackTrace()
                log.error("Encountered an unexpected exception during settings loading.")
                exitProcess(1)
            }

            val service = try {
                Service(settings)
            } catch (ex: Exception) {
                ex.printStackTrace()
                log.error("Encountered an unexpected exception during service build: ${ex.message}.")
                exitProcess(1)
            }

            try {
                service.run()
            } catch (ex: Exception) {
                ex.printStackTrace()
                log.error("Stopping application due to error: ${ex.message}.")
                exitProcess(1)
            }
        }
    }
}
