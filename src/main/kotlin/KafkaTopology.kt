/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.model.Institution
import ch.memobase.model.RecordSet
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import ch.memobase.settings.SettingsLoader
import org.apache.jena.rdf.model.ModelFactory
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.KStream
import org.apache.kafka.streams.kstream.Predicate
import org.apache.logging.log4j.LogManager

class KafkaTopology(private val settings: SettingsLoader) {
    private val log = LogManager.getLogger(this::class.java)
    private val transformer = RdfTransformer(settings.appSettings)
    fun build(): Topology {
        val builder = StreamsBuilder()
        // Process Institutions
        pipeline(
            builder.stream(settings.appSettings.getProperty(Util.institutionInputTopic)),
            settings.appSettings.getProperty(Util.institutionOutputTopic),
            Util.stepInstitution
        )
        // Process Record Sets
        pipeline(builder.stream(settings.inputTopic), settings.outputTopic, Util.stepRecordSet)
        return builder.build()
    }

    private fun pipeline(stream: KStream<String, String>, outputTopic: String, step: String) {
        val parsedJsonStream = stream
            .mapValues { readOnlyKey, value -> JSON.parseJson(readOnlyKey, value, step) }
            .branch(
                Predicate { _, value -> value.second.status == ReportStatus.fatal },
                Predicate { _, value -> value.second.status == ReportStatus.success }
            )

        parsedJsonStream[0]
            .mapValues { value -> value.second.toJson() }
            .to(settings.processReportTopic)

        val processedStream = parsedJsonStream[1]
            .mapValues { value -> value.first!! }
            .mapValues { readOnlyKey, value ->
                try {
                    Pair(
                        when (value) {
                            is Institution -> transformer.createInstitution(value)
                            is RecordSet -> transformer.createRecordSet(value)
                        }, Report(
                            readOnlyKey,
                            ReportStatus.success,
                            "Successfully transformed ${value.type}.",
                            step
                        )
                    )
                } catch (ex: NullPointerException) {
                    if (ex.localizedMessage == "Cannot invoke \"org.memobase.model.Address.getAddress_line1()\" because \"address\" is null") {
                        Pair(
                            Pair("", ModelFactory.createDefaultModel()), Report(
                                readOnlyKey,
                                ReportStatus.fatal,
                                "Institution $readOnlyKey has an empty address element.",
                                step
                            )
                        )
                    } else {
                        Pair(
                            Pair("", ModelFactory.createDefaultModel()), Report(
                                readOnlyKey, ReportStatus.fatal, "Null Pointer Exception: ${ex.localizedMessage}", step
                            )
                        )
                    }
                } catch (ex: Exception) {
                    Pair(
                        Pair("", ModelFactory.createDefaultModel()), Report(
                            readOnlyKey, ReportStatus.fatal, "Unexpected Exception: ${ex.localizedMessage}", step
                        )
                    )
                }
            }

        processedStream
            .mapValues { value -> value.second.toJson() }.to(settings.processReportTopic)

        processedStream.filter { _, value -> value.second.status == ReportStatus.success }
            .mapValues { value -> value.first }
            .map { _, value -> Util.writeModel(value.first, value.second) }
            .to(outputTopic)
    }
}
