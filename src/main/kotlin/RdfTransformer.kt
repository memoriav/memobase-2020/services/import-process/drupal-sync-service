/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.rdf.*
import ch.memobase.model.*
import java.util.Properties
import org.apache.jena.rdf.model.Literal
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory
import org.apache.logging.log4j.LogManager

class RdfTransformer(properties: Properties) {
    private val log = LogManager.getLogger(this::class.java)

    private val cantons = Util.readLabelFile(properties.getProperty(Util.pathCantons))
    private val municipalities = Util.readLabelFile(properties.getProperty(Util.pathMunicipalities))
    private val languages = Util.readLabelFile(properties.getProperty(Util.pathLanguages))

    private val mediaTypes = MediaTypes(
        Translations("other", "Andere", "Andere", "Andere"),
        Translations("sound", "Ton", "Ton", "Ton"),
        Translations("video", "Video", "Video", "Video"),
        Translations("film", "Film", "Film", "Film"),
        Translations("image", "Foto", "Foto", "Foto")
    )


    fun createInstitution(input: Institution): Pair<String, Model> {
        val model = ModelFactory.createDefaultModel()

        val resource = model.createResource(NS.mbcb + input.field_memobase_id)
        resource.addProperty(RDF.type, RICO.CorporateBody)
        if (input.field_memobase_institution == true)
            resource.addProperty(RICO.type, RICO.Types.CorporateBody.memobaseInstitution)
        resource.addProperty(MB.isPublished, booleanLiteral(input.status))

        if (input.field_wikidata_id != null)
            resource.addLiteral(SCHEMA.sameAs, input.field_wikidata_id)
        addIdentifier(model, resource, RICO.Types.Identifier.main, input.field_memobase_id)

        if (input.field_old_memobase_id != null)
            addIdentifier(model, resource, RICO.Types.Identifier.oldMemobase, input.field_old_memobase_id)

        input.recordset_ids.forEach {
            resource.addProperty(RICO.isOrWasHolderOf, model.createResource(NS.mbrs + it))
        }

        resource.addProperty(RICO.name, langLiteral(input.title_de, "de"))
        resource.addProperty(RICO.name, langLiteral(input.title_fr, "fr"))
        resource.addProperty(RICO.name, langLiteral(input.title_it, "it"))

        /* Description */
        addRichTextLiteralIfNotNull(
            resource, listOf(input.field_text_de, input.field_text_fr, input.field_text_it), RICO.descriptiveNote
        )

        input.field_address.forEach { address ->
            if (address != null)
                addLocationResources(resource, model, address)
        }
        input.field_isil.let {
            if (it != null) resource.addProperty(WD.isil, literal(it))
        }
        input.field_email.let {
            if (it != null) {
                resource.addProperty(WD.emailAddress, literal("mailto:$it"))
            }
        }
        input.field_website.let {
            if (it != null) resource.addProperty(WD.website, literal(it.uri))
        }
        input.field_link_archive_catalog.let {
            if (it != null) resource.addProperty(WD.onlineArchive, literal(it.uri))
        }
        input.field_master_institution_types.forEach {
            resource.addProperty(WD.typeOfInstitution, model.createResource(NS.wd + it.substringAfterLast("/")))
        }
        input.field_other_institution_types.let { other_institution_types ->
            if (other_institution_types != null) {
                other_institution_types.forEach {
                    resource.addProperty(WD.typeOfInstitution, model.createResource(NS.wd + it.substringAfterLast("/")))
                }
            }
        }
        if (input.computed_teaser_image_url != null) resource.addLiteral(WD.image, input.computed_teaser_image_url)
        input.field_teaser_color.let {
            if (it != null) resource.addProperty(MB.institutionTeaserColor, literal(it))
        }
        input.computed_teaser_color.let {
            if (it != null) resource.addProperty(MB.institutionComputedTeaserColor, literal(it))
        }

        if (input.field_survey_institution == true && input.collections != null) {
            log.info("Adding avc record set to resource ${input.field_memobase_id}.")
            resource.addProperty(RICO.type, RICO.Types.CorporateBody.memobaseInstitutionInventoryProject)
            createAvkRecordSet(input, model, resource)
        }
        log.info("Transformed institution ${resource.uri}.")
        return Pair(resource.uri, model)
    }

    fun createRecordSet(input: RecordSet): Pair<String, Model> {
        val model = ModelFactory.createDefaultModel()
        val resource = model.createResource(NS.mbrs + input.field_memobase_id)
        resource.addProperty(RDF.type, RICO.RecordSet)
        // Publikations Status
        resource.addProperty(MB.isPublished, booleanLiteral(input.status))

        input.field_processed_teaser_text_de?.let { langLiteral(it.processed, "de") }?.let {
            resource.addLiteral(MB.recordSetProcessedTeaserText, it)
        }
        input.field_processed_teaser_text_fr?.let { langLiteral(it.processed, "fr") }?.let {
            resource.addLiteral(MB.recordSetProcessedTeaserText, it)
        }
        input.field_processed_teaser_text_it?.let { langLiteral(it.processed, "it") }?.let {
            resource.addLiteral(MB.recordSetProcessedTeaserText, it)
        }

        // 0.1 Titel (Memobase)
        addTitle(resource, model, RICO.Types.Title.main, listOf(input.title_de, input.title_fr, input.title_it))
        // + convenience label on the resource directly.
        resource.addProperty(RICO.title, langLiteral(input.title_de, "de"))
        resource.addProperty(RICO.title, langLiteral(input.title_fr, "fr"))
        resource.addProperty(RICO.title, langLiteral(input.title_it, "it"))

        // 0.2.1 Thumbnail Image
        if (input.computed_teaser_image_url != null)
            resource.addProperty(
                WD.image,
                literal(input.computed_teaser_image_url)
            )

        // 1.1 Inhalt
        addRichTextLiteralIfNotNull(
            resource,
            listOf(input.field_content_de, input.field_content_fr, input.field_content_it),
            RICO.scopeAndContent
        )

        // 1.2 Entstehungszeitraum
        // is expected to always be a normalized value YYYY/YYYY.
        input.field_time_period.let { timePeriod ->
            if (timePeriod != null) {
                val date = model.createResource()
                date.addProperty(RDF.type, RICO.DateRange)
                date.addProperty(RICO.normalizedDateValue, timePeriod)
                date.addProperty(RICO.isDateAssociatedWith, resource)
                resource.addProperty(RICO.isAssociatedWithDate, date)
            }
        }
        // 1.3 Sprache
        addLiteralIfNotNull(
            resource,
            listOf(input.field_language_de, input.field_language_fr, input.field_language_it),
            RDA.hasLanguageOfResource
        )

        // 1.4 Zugang Memobase
        addRichTextLiteralIfNotNull(
            resource,
            listOf(input.field_access_memobase_de, input.field_access_memobase_fr, input.field_access_memobase_it),
            RDA.hasRestrictionOnAccess
        )
        // 2.1 Kontext
        addRichTextLiteralIfNotNull(
            resource, listOf(input.field_context_de, input.field_context_fr, input.field_context_it), RICO.history
        )
        // 3.1 Titel
        // Originaltitle des Bestandes
        addTitle(
            resource,
            model,
            RICO.Types.Title.original,
            listOf(input.field_original_title_de, input.field_original_title_fr, input.field_original_title_it)
        )
        // 3.2 Umfang -> recordResourceExtent
        addRichTextLiteralIfNotNull(
            resource,
            listOf(input.field_scope_de, input.field_scope_fr, input.field_scope_it),
            RICO.recordResourceExtent
        )
        // 3.3 Auswahl / Vollständigkeit
        addRichTextLiteralIfNotNull(
            resource,
            listOf(input.field_selection_de, input.field_selection_fr, input.field_selection_it),
            RICO.integrity
        )
        // 3.4 Informationen zur Erschliessung
        addRichTextLiteralIfNotNull(
            resource, listOf(
                input.field_info_on_development_de,
                input.field_info_on_development_fr,
                input.field_info_on_development_it
            ), DC.conformsTo
        )
        // 3.5 Sprache Metadaten Records
        input.field_metadata_language_codes.forEach {
            addLanguage(resource, model, it)
        }
        // 3.6 Rechte
        addRichTextLiteralIfNotNull(
            resource, listOf(
                input.field_rights_de, input.field_rights_fr, input.field_rights_it
            ), RICO.conditionsOfUse
        )
        // 3.7 Original ID
        input.field_original_id.let { originalIdentifier ->
            if (originalIdentifier != null) {
                addIdentifier(model, resource, RICO.Types.Identifier.original, originalIdentifier)
            }
        }
        // 3.8 Original Signatur
        input.field_original_shelf_mark.let { callNumber ->
            if (callNumber != null) {
                addIdentifier(model, resource, RICO.Types.Identifier.callNumber, callNumber)
            }
        }
        // 3.9 Original Beschreibung (Text)
        // 3.9.1 Original Beschreibung (Link)
        addOriginalRecordSetLink(
            resource, model, listOf(
                input.field_original_description_de,
                input.field_original_description_fr,
                input.field_original_description_it
            )
        )
        // TODO: CONTINUE HERE FOR RUST IMPLEMENTATION
        // 4.1 Zugang
        addRichTextLiteralIfNotNull(
            resource,
            listOf(input.field_access_de, input.field_access_fr, input.field_access_it),
            RICO.conditionsOfAccess
        )
        // 4.2 Zuständige Institution (Original)
        addRelatedInstitution(resource, model, RICO.Types.CorporateBody.original, input.field_resp_institution_original)
        // 4.3 Zuständige Institution (Master)
        addRelatedInstitution(resource, model, RICO.Types.CorporateBody.master, input.field_resp_institution_master)
        // 4.4 Zuständige Institution (Access)
        addRelatedInstitution(resource, model, RICO.Types.CorporateBody.access, input.field_resp_institution_access)

        // Currently each project link is added as a separate entity.
        // 5.1 Projekt (Titel)
        // 5.2 Projekt (Link)
        val projects = combineLinkFields(input.field_project_de, input.field_project_fr, input.field_project_it)
        projects.forEach { linkWithLanguage ->
            addMemoriavProject(resource, model, linkWithLanguage)
        }

        // Currently each record set link is added as a separate entity.
        // TODO: This is completely bugged...
        // 5.3 Verwandte Bestände (Titel)
        // 5.4 Verwandte Bestände (Link)
        val relatedRecordSets = combineLinkFields(
            input.field_related_record_sets_de,
            input.field_related_record_sets_fr,
            input.field_related_record_sets_it
        )
        relatedRecordSets.forEach { linkWithLanguage: LinkWithLanguage ->
            if (linkWithLanguage.title == Util.internalRelatedRecordSet) {
                if (!resource.hasProperty(
                        RICO.isRecordResourceAssociatedWithRecordResource,
                        model.createResource(NS.mbrs + linkWithLanguage.uri)
                    )
                ) {
                    resource.addProperty(
                        RICO.isRecordResourceAssociatedWithRecordResource,
                        model.createResource(NS.mbrs + linkWithLanguage.uri)
                    )
                }
            } else {
                addRelatedRecordSet(resource, model, linkWithLanguage)
            }
        }

        // Currently each publication link is added as a separate entity.
        // 5.5 Publikationen (Titel)
        // 5.6 Publikationen (Link)
        val publications = combineLinkFields(
            input.field_publications_de, input.field_publications_fr, input.field_publications_it
        )
        publications.forEach {
            addRelatedRecord(
                resource,
                model,
                it,
                RICO.isOrWasSubjectOf,
                RICO.hasOrHadSubject,
                RICO.Types.Record.publication
            )
        }
        // Currently each record link is added as a separate entity.
        // 5.7 Dokumente (Titel)
        // 5.8 Dokumente (Link)
        val documents = combineLinkFields(
            input.field_documents_de, input.field_documents_fr, input.field_documents_it
        )
        documents.forEach {
            addRelatedRecord(
                resource, model, it, RICO.isRecordResourceAssociatedWithRecordResource,
                RICO.isRecordResourceAssociatedWithRecordResource, RICO.Types.Record.related
            )
        }
        // 6.1 Datenübernahme
        addRichTextLiteralIfNotNull(
            resource,
            listOf(input.field_data_transfer_de, input.field_data_transfer_fr, input.field_data_transfer_it),
            RICO.descriptiveNote
        )
        // 6.2 Datum der Übernahme in Memobase
        input.field_transfer_date.let { date ->
            if (date != null) {
                resource.addLiteral(RICO.publicationDate, dateLiteral(model, date.split("T")[0]))
            }
        }
        // 6.3 Datum letzte Aktualisierung in Memobase
        val dateLiteralNow = model.createTypedLiteral(Util.now, XSD.dateTime)
        resource.addLiteral(RICO.modificationDate, dateLiteralNow)
        // 6.4 Memobase ID
        addIdentifier(model, resource, RICO.Types.Identifier.main, input.field_memobase_id)

        // 8.1 Unterstützt durch Memoriav
        if (input.field_supported_by_memoriav) {
            resource.addProperty(RDA.hasSponsoringAgentOfResource, model.createResource(MB.memoriavInstitutionURI))
        }
        // 8.2 Institution
        input.field_institution.forEach {
            resource.addProperty(RICO.hasOrHadHolder, model.createResource(NS.mbcb + it))
        }
        // 9.3 Alte Memobase ID
        if (input.field_old_memobase_id != null)
            addIdentifier(model, resource, RICO.Types.Identifier.oldMemobase, input.field_old_memobase_id)

        log.info("Transformed record set ${resource.uri}.")
        return Pair(resource.uri, model)
    }

    private fun createAvkRecordSet(input: Institution, model: Model, institution: Resource) {
        val id = input.field_memobase_id + "-avk"
        val resource = model.createResource(NS.mbrs + id)
        resource.addProperty(MB.publicationPermitted, booleanLiteral(input.field_private == true))
        resource.addProperty(RDF.type, RICO.RecordSet)
        resource.addProperty(
            RICO.title,
            langLiteral("Sammlung Audiovisuelles Kulturgut ${input.title_de}", "de")
        )
        resource.addProperty(
            RICO.title,
            langLiteral("Sammlung Audiovisuelles Kulturgut ${input.title_fr}", "fr")
        )
        resource.addProperty(
            RICO.title,
            langLiteral("Sammlung Audiovisuelles Kulturgut ${input.title_it}", "it")
        )
        addIdentifier(model, resource, RICO.Types.Identifier.main, id)
        addTitle(
            resource, model, RICO.Types.Title.main, listOf(
                "Sammlung Audiovisuelles Kulturgut ${input.title_de}",
                "Sammlung Audiovisuelles Kulturgut ${input.title_fr}",
                "Sammlung Audiovisuelles Kulturgut ${input.title_it}"
            )
        )
        addHolder(resource, institution)
        addLiteralIfNotNull(
            resource,
            listOf(input.field_accessibility_de, input.field_accessibility_fr, input.field_accessibility_it),
            RICO.conditionsOfAccess
        )
        input.field_survey_date.let {
            if (it != null) {
                val date = it.split("T")[0]
                resource.addProperty(RICO.publicationDate, dateLiteral(model, date))
            }
        }

        input.collections?.Andere.let { mediaCollection ->
            if (mediaCollection != null)
                createMediaTypeRecordSet(input, model, mediaCollection, resource, mediaTypes.other)
        }
        input.collections?.Ton.let { mediaCollection ->
            if (mediaCollection != null)
                createMediaTypeRecordSet(input, model, mediaCollection, resource, mediaTypes.sound)
        }
        input.collections?.Video.let { mediaCollection ->
            if (mediaCollection != null)
                createMediaTypeRecordSet(input, model, mediaCollection, resource, mediaTypes.video)
        }
        input.collections?.Film.let { mediaCollection ->
            if (mediaCollection != null)
                createMediaTypeRecordSet(input, model, mediaCollection, resource, mediaTypes.film)
        }
        input.collections?.Foto.let { mediaCollection ->
            if (mediaCollection != null)
                createMediaTypeRecordSet(input, model, mediaCollection, resource, mediaTypes.image)
        }
    }

    private fun createMediaTypeRecordSet(
        input: Institution,
        model: Model,
        mediaCollection: MediaCollection,
        avkRecordSet: Resource,
        mediaType: Translations
    ) {
        val id = input.field_memobase_id + "-" + mediaType.key
        val mediaRecordSet = model.createResource(NS.mbrs + id)
        mediaRecordSet.addProperty(RDF.type, RICO.RecordSet)
        mediaRecordSet.addProperty(
            RICO.title,
            langLiteral("Sammlung ${mediaType.de} ${input.title_de}", "de")
        )
        mediaRecordSet.addProperty(
            RICO.title,
            langLiteral("Sammlung ${mediaType.fr} ${input.title_fr}", "fr")
        )
        mediaRecordSet.addProperty(
            RICO.title,
            langLiteral("Sammlung ${mediaType.it} ${input.title_it}", "it")
        )
        addIdentifier(model, mediaRecordSet, RICO.Types.Identifier.main, id)
        addTitle(
            mediaRecordSet, model, RICO.Types.Title.main, listOf(
                "Sammlung ${mediaType.de} ${input.title_de}",
                "Sammlung ${mediaType.de} ${input.title_fr}",
                "Sammlung ${mediaType.de} ${input.title_it}"
            )
        )
        mediaCollection.field_physical_number.let {
            if (it != null)
                addExtent(
                    mediaRecordSet,
                    model,
                    RICO.Types.Extent.physicalFormats,
                    it,
                    RICO.UnitOfMeasurement.amountEstimated
                )
        }
        mediaCollection.field_digital_number.let {
            if (it != null)
                addExtent(
                    mediaRecordSet,
                    model,
                    RICO.Types.Extent.digitalFormats,
                    it,
                    RICO.UnitOfMeasurement.amountEstimated
                )
        }
        mediaCollection.field_indexed.let {
            if (it != null)
                addExtent(
                    mediaRecordSet,
                    model,
                    RICO.Types.Extent.catalogedShare,
                    it,
                    RICO.UnitOfMeasurement.percentageEstimated
                )
        }
        mediaCollection.field_digitized.let {
            if (it != null)
                addExtent(
                    mediaRecordSet,
                    model,
                    RICO.Types.Extent.digitizedShare,
                    it,
                    RICO.UnitOfMeasurement.percentageEstimated
                )
        }
        for (item in mediaCollection.field_time)
            addDate(mediaRecordSet, model, item)
        for (item in mediaCollection.field_subjects)
            addSubjects(mediaRecordSet, model, item)
        for (item in mediaCollection.field_geo)
            mediaRecordSet.addProperty(RDA.hasCoverageOfContent, langLiteral(item, "de"))
        addContentType(mediaRecordSet, model, mediaType)
        mediaRecordSet.addProperty(RICO.isOrWasPartOf, avkRecordSet)
        avkRecordSet.addProperty(RICO.hasOrHadPart, mediaRecordSet)
    }

    private fun addIdentifier(model: Model, resource: Resource, type: String, value: String) {
        val identifier = model.createResource()
        identifier.addProperty(RDF.type, RICO.Identifier)
        type.let {
            if (it.isNotEmpty()) {
                identifier.addProperty(RICO.type, literal(it))
            }
        }
        identifier.addProperty(RICO.identifier, literal(value))
        identifier.addProperty(RICO.isOrWasIdentifierOf, resource)
        resource.addProperty(RICO.hasOrHadIdentifier, identifier)
    }

    private fun addExtent(resource: Resource, model: Model, type: String, quantity: Int, unitOfMeasurement: String) {
        val extend = model.createResource()
        extend.addProperty(RDF.type, RICO.Extent)
        extend.addProperty(RICO.type, type)
        extend.addProperty(RICO.quantity, integerLiteral(quantity))
        extend.addProperty(RICO.unitOfMeasurement, literal(unitOfMeasurement))
        extend.addProperty(RICO.isExtentOf, resource)
        resource.addProperty(RICO.hasExtent, extend)
    }

    private fun addDate(resource: Resource, model: Model, value: String) {
        val date = model.createResource()
        date.addProperty(RDF.type, RICO.DateRange)
        date.addProperty(RICO.expressedDate, langLiteral(value, "de"))
        date.addProperty(RICO.isDateAssociatedWith, resource)
        resource.addProperty(RICO.isAssociatedWithDate, date)
    }

    private fun addSubjects(resource: Resource, model: Model, value: String) {
        val subject = model.createResource()
        subject.addProperty(RDF.type, SKOS.Concept)
        subject.addProperty(SKOS.prefLabel, langLiteral(value, "de"))
        subject.addProperty(RICO.isOrWasSubjectOf, resource)
        resource.addProperty(RICO.hasOrHadSubject, subject)
    }

    private fun addHolder(resource: Resource, holder: Resource) {
        resource.addProperty(RICO.hasOrHadHolder, holder)
        holder.addProperty(RICO.isOrWasHolderOf, resource)
    }

    private fun addLanguage(resource: Resource, model: Model, value: String) {
        val language = model.createResource()
        language.addProperty(RDF.type, RICO.Language)
        language.addProperty(RICO.type, literal(RICO.Types.Language.metadata))
        languages[value].let {
            if (it == null) {
                language.addProperty(RICO.name, literal(value))
            } else {
                language.addProperty(RICO.name, langLiteral(it.de, "de"))
                language.addProperty(RICO.name, langLiteral(it.fr, "fr"))
                language.addProperty(RICO.name, langLiteral(it.it, "it"))
                language.addProperty(SCHEMA.sameAs, literal(it.id))
            }
        }
        language.addProperty(RICO.isOrWasLanguageOf, resource)
        resource.addProperty(RICO.hasOrHadLanguage, language)
    }

    /**
     * titles: A list of the titles in each language. Order: `de`, `fr`, `it`. See `getLanguage()`.
     */
    private fun addTitle(resource: Resource, model: Model, type: String, titles: List<String?>) {
        if (titles.all { it == null }) return
        val title = model.createResource()
        title.addProperty(RDF.type, RICO.Title)
        title.addProperty(RICO.type, literal(type))
        titles.forEachIndexed { index, s ->
            if (s != null) title.addProperty(RICO.title, langLiteral(s, getLanguage(index)))
        }
        title.addProperty(RICO.isOrWasTitleOf, resource)
        resource.addProperty(RICO.hasOrHadTitle, title)
    }

    private fun addContentType(resource: Resource, model: Model, translations: Translations) {
        val contentType = model.createResource()
        contentType.addProperty(RDF.type, RICO.ContentType)
        contentType.addProperty(RICO.name, langLiteral(translations.de, "de"))
        contentType.addProperty(RICO.name, langLiteral(translations.fr, "fr"))
        contentType.addProperty(RICO.name, langLiteral(translations.it, "it"))
        contentType.addProperty(RICO.isOrWasContentTypeOfAllMembersOf, resource)
        resource.addProperty(RICO.hasOrHadAllMembersWithContentType, contentType)
    }

    private fun combineLinkFields(de: List<Link>, fr: List<Link>, it: List<Link>): List<LinkWithLanguage> {
        return de.map { LinkWithLanguage(it.uri, it.title, "de") } + fr.map {
            LinkWithLanguage(
                it.uri,
                it.title,
                "fr"
            )
        } + it.map {
            LinkWithLanguage(it.uri, it.title, "it")
        }
    }

    private fun addMemoriavProject(resource: Resource, model: Model, link: LinkWithLanguage) {
        val sponsoringAgents = resource.listProperties(RDA.hasSponsoringAgentOfResource).filterKeep {
            val statment = model.createStatement(it.`object`.asResource(), SCHEMA.sameAs, literal(link.uri))
            val result = model.contains(statment)
            result
        }.toList()

        if (sponsoringAgents.isEmpty()) {
            val node = model.createResource()
            node.addProperty(RDF.type, RICO.CorporateBody)
            node.addProperty(RICO.type, RICO.Types.CorporateBody.memoriavProject)
            link.title.let {
                if (!it.isNullOrEmpty()) node.addProperty(RICO.title, langLiteral(it, link.language))
            }
            node.addProperty(SCHEMA.sameAs, literal(link.uri))
            node.addProperty(RDA.hasSponsoredResourceOfAgent, resource)
            resource.addProperty(RDA.hasSponsoringAgentOfResource, node)
        } else {
            if (!link.title.isNullOrEmpty())
                sponsoringAgents[0].`object`.asResource()
                    .addProperty(RICO.title, langLiteral(link.title, link.language))
        }
    }

    private fun addRelatedRecordSet(resource: Resource, model: Model, link: LinkWithLanguage) {
        val relatedRecordSets = resource.listProperties(RICO.isRecordResourceAssociatedWithRecordResource).filterKeep {
            model.contains(model.createStatement(it.`object`.asResource(), SCHEMA.sameAs, literal(link.uri)))
        }.toList()
        if (relatedRecordSets.isEmpty()) {
            val node = model.createResource()
            node.addProperty(RDF.type, RICO.RecordSet)
            link.title.let {
                if (!it.isNullOrEmpty()) node.addProperty(RICO.title, langLiteral(it, link.language))
            }
            node.addProperty(RICO.type, literal(RICO.Types.RecordSet.related))
            node.addProperty(SCHEMA.sameAs, literal(link.uri))
            node.addProperty(RICO.isRecordResourceAssociatedWithRecordResource, resource)
            resource.addProperty(RICO.isRecordResourceAssociatedWithRecordResource, node)
        } else {
            if (!link.title.isNullOrEmpty())
                relatedRecordSets[0].`object`.asResource()
                    .addProperty(RICO.title, langLiteral(link.title, link.language))
        }
    }

    private fun addRelatedRecord(
        resource: Resource, model: Model, link: LinkWithLanguage, property: Property,
        inverseProperty: Property, type: String
    ) {
        val relatedRecords = resource.listProperties(property).filterKeep {
            model.contains(model.createStatement(it.`object`.asResource(), RDF.type, RICO.Record))
        }.filterKeep {
            model.contains(model.createStatement(it.`object`.asResource(), SCHEMA.sameAs, literal(link.uri)))
        }.toList()
        if (relatedRecords.isEmpty()) {
            val relatedRecord = model.createResource()
            relatedRecord.addProperty(RDF.type, RICO.Record)
            link.title.let { title ->
                if (!title.isNullOrEmpty()) relatedRecord.addProperty(RICO.title, langLiteral(title, link.language))
            }
            relatedRecord.addProperty(RICO.type, literal(type))
            relatedRecord.addProperty(SCHEMA.sameAs, literal(link.uri))
            relatedRecord.addProperty(inverseProperty, resource)
            resource.addProperty(property, relatedRecord)
        } else {
            if (!link.title.isNullOrEmpty())
                relatedRecords[0].`object`.asResource()
                    .addProperty(RICO.title, langLiteral(link.title, link.language))
        }
    }

    /**
     * Does not currently add inverse properties for WD.adminUnit.
     */
    private fun addLocationResources(resource: Resource, model: Model, address: Address) {
        val location = model.createResource()
        location.addProperty(RDF.type, RICO.Place)
        if (address.main == true) {
            location.addProperty(RICO.type, literal("mainAddress"))
        } else {
            location.addProperty(RICO.type, literal("additionalAddress"))
        }
        val streetAddress = address.address_line1
        val secondAddressLine = address.address_line2
        val combinedStreetAddress = if (secondAddressLine.isNullOrEmpty()) {
            streetAddress
        } else {
            streetAddress + "\n" + secondAddressLine
        }
        val streetNumber = streetAddress.substringAfterLast(" ")
        val street = streetAddress.replace(streetNumber, "").trim()
        location.addProperty(WD.street, literal(street))
        location.addProperty(WD.streetNumber, literal(streetNumber))
        location.addProperty(WD.streetAddress, literal(combinedStreetAddress))


        val postalCode = address.postal_code.trim()
        location.addProperty(WD.postalCode, literal(postalCode))

        location.addProperty(WD.coordinates, literal(address.coordinates))

        val canton = model.createResource()
        canton.addProperty(RDF.type, RICO.Place)
        location.addProperty(WD.adminUnit, canton)
        cantons[address.administrative_area].let {
            // this should always be the case!
            if (it != null) {
                canton.addProperty(RICO.name, langLiteral(it.de, "de"))
                canton.addProperty(RICO.name, langLiteral(it.fr, "fr"))
                canton.addProperty(RICO.name, langLiteral(it.it, "it"))
                canton.addProperty(SCHEMA.sameAs, it.id)
            } else {
                // this shouldn't happen!
                canton.addProperty(RICO.name, literal("Unknown"))
            }
            canton.addProperty(RICO.type, literal("canton"))
        }

        val municipality = model.createResource()
        municipality.addProperty(RDF.type, RICO.Place)
        location.addProperty(WD.adminUnit, municipality)
        municipalities[postalCode].let {
            if (it != null) {
                municipality.addProperty(RICO.name, langLiteral(it.de, "de"))
                municipality.addProperty(RICO.name, langLiteral(it.fr, "fr"))
                municipality.addProperty(RICO.name, langLiteral(it.it, "it"))
                municipality.addProperty(SCHEMA.sameAs, it.id)
            } else {
                municipality.addProperty(RICO.name, literal(address.locality))
            }
            municipality.addProperty(RICO.type, "municipality")
        }
        // country is currently hard coded to switzerland!
        location.addProperty(WD.country, WD.switzerland)

        location.addProperty(RICO.isOrWasLocationOf, resource)
        resource.addProperty(RICO.hasOrHadLocation, location)
    }

    private fun langLiteral(text: String, language: String): Literal =
        ResourceFactory.createLangLiteral(text.trim(), language)

    private fun literal(text: String): Literal = ResourceFactory.createPlainLiteral(text.trim())
    private fun dateLiteral(model: Model, text: String): Literal = model.createTypedLiteral(text.trim(), XSD.date)
    private fun integerLiteral(value: Int): Literal = ResourceFactory.createTypedLiteral(value)

    private fun booleanLiteral(value: Boolean): Literal = ResourceFactory.createTypedLiteral(value)

    private fun addRichTextLiteralIfNotNull(resource: Resource, field: List<RichText?>, property: Property) {
        field.forEachIndexed { index, s ->
            if (s != null) resource.addProperty(property, langLiteral(s.value, getLanguage(index)))
        }
    }

    private fun getLanguage(index: Int): String {
        return when (index) {
            0 -> "de"
            1 -> "fr"
            2 -> "it"
            else -> "un"
        }
    }

    private fun addLiteralIfNotNull(resource: Resource, fields: List<String?>, property: Property) {
        fields.forEachIndexed { index, s ->
            if (s != null) resource.addProperty(property, langLiteral(s, getLanguage(index)))
        }
    }

    private fun addOriginalRecordSetLink(resource: Resource, model: Model, links: List<Link?>) {
        if (links.all { it == null }) return
        val recordSet = model.createResource()
        recordSet.addProperty(RDF.type, RICO.RecordSet)
        links.forEachIndexed { index, link ->
            if (link != null) {
                recordSet.addLiteral(SCHEMA.sameAs, langLiteral(link.uri, getLanguage(index)))
                if (!link.title.isNullOrEmpty()) {
                    recordSet.addLiteral(RICO.title, langLiteral(link.title, getLanguage(index)))
                }
            }
        }
        recordSet.addProperty(RICO.isSourceOf, resource)
        resource.addProperty(RICO.hasSource, recordSet)
    }

    private fun addRelatedInstitution(resource: Resource, model: Model, type: String, item: List<String>) {
        item.forEach {
            val relatedInstitution = model.createResource()
            relatedInstitution.addProperty(RDF.type, RICO.RecordResourceHoldingRelation)
            relatedInstitution.addProperty(RICO.type, type)
            relatedInstitution.addProperty(
                RICO.recordResourceHoldingRelationHasSource,
                model.createResource(NS.mbcb + it)
            )

            relatedInstitution.addProperty(RICO.recordResourceHoldingRelationHasTarget, resource)
            resource.addProperty(
                RICO.recordResourceOrInstantiationIsTargetOfRecordResourceHoldingRelation,
                relatedInstitution
            )
        }
    }
}