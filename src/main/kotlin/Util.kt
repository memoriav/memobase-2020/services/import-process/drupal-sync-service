/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.model.IdLabels
import ch.memobase.settings.SettingsLoader
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import java.io.File
import java.io.FileNotFoundException
import java.io.StringWriter
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.system.exitProcess
import org.apache.jena.rdf.model.Model
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.apache.kafka.streams.KeyValue
import org.apache.logging.log4j.LogManager

object Util {
    const val pathLanguages = "path.languages"
    const val pathCantons = "path.cantons"
    const val pathMunicipalities = "path.municipalities"
    const val institutionInputTopic = "institutionInputTopic"
    const val institutionOutputTopic = "institutionOutputTopic"

    const val drupalInstitutionTypeIdentifier = "node--institution"
    const val drupalRecordSetTypeIdentifier = "node--record_set"
    const val stepRecordSet = "RS-01-drupal-to-rdf"
    const val stepInstitution = "IS-01-drupal-to-rdf"

    /**
     * This constant is used to recognize if the related record set is a record set indexed in Memobase.
     * The value is set to internal by the import api. So do not change unless it is changed in the API too.
     */
    const val internalRelatedRecordSet = "internal"
    val now: String = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)
    private val log = LogManager.getLogger(this::class.java)

    private val csv = csvReader()

    fun readLabelFile(path: String): Map<String, IdLabels> {
        try {
            val labelList = csv.readAll(File(path))
            val labelsMap = mutableMapOf<String, IdLabels>()
            for (row in labelList.listIterator(1)) {
                val code = row[0].trim()
                val id = row[1].trim()
                if (code.contains(",")) {
                    code.split(",").map {
                        it.trim()
                    }.forEach {
                        labelsMap[it] = IdLabels(id, row[2].trim(), row[3].trim(), row[4].trim())
                    }
                } else {
                    labelsMap[code] = IdLabels(id, row[2].trim(), row[3].trim(), row[4].trim())
                }
            }
            return labelsMap
        } catch (ex: FileNotFoundException) {
            log.error(ex.localizedMessage)
            exitProcess(1)
        }
    }

    fun writeModel(uri: String, model: Model): KeyValue<String, String> {
        return StringWriter().use { writer ->
            RDFDataMgr.write(writer, model, RDFFormat.NTRIPLES_UTF8)
            KeyValue(uri, writer.toString().trim())
        }
    }

    fun loadSettings(file: String): SettingsLoader {
        return SettingsLoader(
            listOf(
                pathMunicipalities,
                pathCantons,
                pathLanguages,
                institutionInputTopic,
                institutionOutputTopic
            ),
            file,
            useStreamsConfig = true
        )
    }
}