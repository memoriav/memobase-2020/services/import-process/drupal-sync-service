## Drupal Sync Service

Transforms json objects into RDF Resources.

[Internal Confluence Documentation](https://ub-basel.atlassian.net/wiki/spaces/MEMOBASE/pages/758808582/Step+01+Drupal+to+RDF)

### Canton Query

Here the codes were added manually and the names adjusted to exclude the word canton. Just kept the name.

https://query.wikidata.org/

```sparql
SELECT ?item ?de ?fr ?it
WHERE 
{
  ?item wdt:P31 wd:Q23058.
  ?item rdfs:label ?de .
  FILTER(LANG(?de) = "de")
  
  ?item rdfs:label ?fr .
  FILTER(LANG(?fr) = "fr")
  
  ?item rdfs:label ?it .
  FILTER(LANG(?it) = "it")
}
```

### Municipalities Query

https://query.wikidata.org/

```sparql
SELECT (GROUP_CONCAT(DISTINCT ?postalCode; SEPARATOR=",") as ?code) ?id (SAMPLE(?de) as ?de) (SAMPLE(?fr) as ?fr) (SAMPLE(?it) as ?it) 
WHERE 
{
  ?id wdt:P31 wd:Q70208. # swiss municipalities
  ?id wdt:P281 ?postalCode .
  
  ?id rdfs:label ?de .
  FILTER(LANG(?de) = "de")
  
  ?id rdfs:label ?fr .
  FILTER(LANG(?fr) = "fr")
  
  ?id rdfs:label ?it .
  FILTER(LANG(?it) = "it")
  
  MINUS {
    ?id wdt:P31 wd:Q685309 # removes former municipalities
  }
} 
GROUP BY ?id
```

### More Complete Query

```sql
SELECT ?item (GROUP_CONCAT(DISTINCT ?postalCode; SEPARATOR=",") as ?groupedPostalCode) ?canton ?labelDE ?labelFR ?labelIT (GROUP_CONCAT(DISTINCT ?coordinates; SEPARATOR=",") as ?groupedCoordinates)
WHERE 
{
  ?item wdt:P31 wd:Q70208. # swiss municipalities
  OPTIONAL { ?item wdt:P281 ?postalCode . }
  ?item wdt:P625 ?coordinates .
  OPTIONAL { 
    ?item wdt:P131 ?canton .
    ?canton wdt:P31 wd:Q23058 . # adminUnit is canton of switzerland 
  }
  OPTIONAL {
    ?item rdfs:label ?labelDE .
    FILTER(LANG(?labelDE) = "de")
  }

  OPTIONAL {
    ?item rdfs:label ?labelFR .
    FILTER(LANG(?labelFR) = "fr")
  }
  
  OPTIONAL {
    ?item rdfs:label ?labelIT .
    FILTER(LANG(?labelIT) = "it")
  }
  MINUS {
    ?item wdt:P31 wd:Q685309 # removes former municipalities
  }
  
}
GROUP BY ?item ?canton ?labelDE ?labelFR ?labelIT
```