FROM gradle:7.4.2-jdk17-alpine
ADD . /
WORKDIR /
RUN gradle --no-daemon --no-scan --no-build-cache distTar
RUN cd /build/distributions && tar xf app.tar

FROM openjdk:17-slim
COPY --from=0 /build/distributions/app /app
CMD /app/bin/drupal-sync-service
